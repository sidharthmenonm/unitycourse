import jQuery from "jquery";
import slick from "slick-carousel";

jQuery(document).ready(function() {
  $('[data-youtube]').youtube_background();
});

jQuery('#carousal-ecosystem').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000
});