import * as THREE from 'three';
import GLTFLoader from 'three-gltf-loader';

let container;
let camera;
let renderer;
let scene;
let house;

function init() {
  container = document.querySelector(".scene");
  container.classList.add("loading");
  //Create scene
  scene = new THREE.Scene();

  const fov = 35;
  const aspect = container.clientWidth / container.clientHeight;
  const near = 0.1;
  const far = 2000;

  //Camera setup
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  // camera.position.set(0, 2, 20);
  camera.position.set(0, 2, 30);

  const ambient = new THREE.AmbientLight(0x404040, 2);
  scene.add(ambient);

  const light = new THREE.DirectionalLight(0x28a745, 3);
  light.position.set(50, 50, 50);
  scene.add(light);

  const light1 = new THREE.DirectionalLight(0xffffff, 2);
  light1.position.set(0, 0, 10);
  scene.add(light1);

  //Renderer
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setSize(container.clientWidth, container.clientHeight);
  renderer.setPixelRatio(window.devicePixelRatio);

  container.appendChild(renderer.domElement);

  //Load Model
  const loader = new GLTFLoader();
  loader.load("./htc_vive/scene.gltf", function(gltf) {
    container.classList.remove("loading");
    scene.add(gltf.scene);
    house = gltf.scene.children[0];
    animate();
  });
}

function animate() {
  requestAnimationFrame(animate);
  house.rotation.z += 0.005;
  // house.rotation.y += 0.005;
  // house.rotation.x += 0.005;
  renderer.render(scene, camera);
}

init();

function onWindowResize() {
  camera.aspect = container.clientWidth / container.clientHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(container.clientWidth, container.clientHeight);
}

window.addEventListener("resize", onWindowResize);